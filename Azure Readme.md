# Downing JCR Comms Email Generator

Please **clone this project** then launch `main.ipynb` to get started.

In addition to the variables in `main.ipynb`, `items.html` must be filled in before generating the email. The output is stored in `out.html`, and previously generated emails in the `archive` folder.

For information about this project, please click [here](https://gitlab.com/m-c-moore/downing-jcr-comms-email).

> This program was put together by [Matt Moore](https://gitlab.com/m-c-moore), and is based on [this project](https://github.com/dowjcr/comms-email) by [Cam O'Connor](https://github.com/cjoc).