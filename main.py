intro = """
"""

meme_link = ""
meme_caption = ""

from EmailGenerator import EmailGenerator

eg = EmailGenerator()

eg.write_preamble()
eg.write_intro(intro)
eg.write_meme(meme_link, meme_caption)
eg.write_items(check_links=True)
eg.write_postamble()

eg.save_output()
