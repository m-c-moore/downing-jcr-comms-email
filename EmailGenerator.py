import datetime, os, re, shutil, urllib.request


class EmailGenerator(object):
    def __init__(self):
        self.date = datetime.datetime.now()
        self.out = open("out.html", "w")

    def _clean_text(self, text: str) -> str:
        """Return text with line breaks and extra spaces removed,
        and smart quotes replaced by plain quotes."""

        to_replace = {
            "\n": " ",
            "\r": " ",
            "’": "'",
            "‘": "'",
            "â€™": "'",
            "â€˜": "'",
            "â€“": "-",
            "“": '"',
            "”": '"',
        }

        for k, v in to_replace.items():
            text = text.replace(k, v)

        text = text.replace("  ", " ").strip()
        return text

    def _check_link(self, link: str) -> None:
        """Print warning if:
            - the link is not a valid url or a 'mailto:' link,
            - the email in the 'mailto:' link does not match a (basic) email regex pattern,
            - an error occurs when making a `urllib` request to that url.
        """

        if link[:7] == "mailto:":
            email = link[7:]
            email_pattern = r"^((?!\.)[\w\-_.]*[^.])(@[\w\-\.]+)([^.\W])$"

            if re.match(email_pattern, email) is None:
                print(f"\tThe email '{email}' in the 'mailto:' link may be invalid.")

        elif link[:8] == "https://" or link[:7] == "http://" or link[:4] == "www.":
            try:
                urllib.request.urlopen(link).read()
            except Exception as e:
                print(
                    "\tThe link '{:.30}{}' may be invalid.\n\t\tError message: '{}'".format(
                        link, "..." if len(link) > 30 else "", str(e)
                    )
                )

        else:
            print(
                f"\tThe link 'f{link}' should be a valid url or start with 'mailto:'."
            )

    def check_links(self, text: str) -> None:
        """Extract links from the 'href=' attribute of '<a>...</a>' tags,
        and use the `self._check_link` method to validate them."""

        href_pattern = r"<a\s+(?:[^>]*?\s+)?href=([\"'])(.*?)\1"
        matches = re.findall(href_pattern, text)

        for match in matches:
            link = match[1]
            self._check_link(link)

    def write_preamble(self) -> None:
        """Write styles, preamble and header to the output file.
        Should be run first."""

        with open("html/style.html", "r") as f:
            self.out.write(f.read())

        with open("html/preamble.html", "r") as f:
            self.out.write(f.read())

        with open("html/header.html", "r") as f:
            header = f.read() % self.date.strftime("%d/%m/%Y")
            self.out.write(header)

    def write_intro(self, intro: str) -> None:
        """Write given intro to the output file. Should be run second.
        
        This intro text can contain html, e.g. `<br>` for line breaks, 
        `<a>...</a>` for links and `<b>...</b>` / `<i>...</i>`
        for formatting."""

        intro = self._clean_text(intro)
        intro = f"<br><br><p>{intro}<br><br></p>"
        self.out.write(intro)

    def write_meme(self, link: str, caption: str) -> None:
        """Write the meme image (given a link) and caption (can be an
        empty string) to the output file. Should be run third.
        
        This caption can contain html, e.g. `<br>` for line breaks, 
        `<a>...</a>` for links and `<b>...</b>` / `<i>...</i>`
        for formatting. By default it is wrapped in `<i>...</i>`."""

        caption = self._clean_text(caption)

        with open("html/meme.html", "r") as f:
            meme = f.read() % (link, caption)
            self.out.write(meme)

    def write_items(self, check_links: bool = True) -> None:
        """Write the items to the output file. If `check_links = True`, 
        will print a warning if `<a>...</a>` contain an invalid 'href='.
        Should be run fourth.
        
        The items are read from the file `items.html`, which must be
        in the format:
        ```txt
        title of item 1
        body of item 1
        more about item 1
        
        =====
        title of item 2
        body of item 2
        more about item 2

        =====
        etc.
        ```
        This file can contain html, e.g. `<br>` for line breaks, 
        `<a>...</a>` for links and `<b>...</b>` / `<i>...</i>`
        for formatting.
        """

        with open("items.html", "r") as f:
            lines = f.readlines()

        c = 0
        titles = []
        bodies = [""]
        is_title = True

        for line in lines:
            line = self._clean_text(line)

            if line == "=====":
                is_title = True
                c += 1

            elif is_title:
                titles.append(line)
                bodies.append("")
                is_title = False

            else:
                bodies[c] = bodies[c] + line

        if check_links:
            [self.check_links(body) for body in bodies]

        # write table of contents
        self.out.write("</div><h2>In this week's email...</h2>")

        for i, title in enumerate(titles):
            entry = f'<p><a href="#anchor{i+1}">{i+1}. {title}</a></p>'
            self.out.write(entry)

        # write items
        for i, (title, body) in enumerate(zip(titles, bodies)):
            entry = f'<div id="anchor{i+1}"><p style="padding-left:50px">'
            entry += f"<h3>{i+1}. {title}</h3>"
            entry += f"<p>{body}</p></p></div>"

            self.out.write(entry)

    def write_postamble(self) -> None:
        """Write the postamble to the output file. Should be run fifth."""

        with open("html/postamble.html", "r") as f:
            self.out.write(f.read())

    def save_output(self) -> None:
        """Close the output file, and copy it to the archive folder.
        This should be run last."""
        self.out.close()

        date = self.date.strftime("%Y-%m-%d")

        try:
            os.makedirs("archive")
        except FileExistsError:
            pass

        shutil.copyfile("out.html", f"archive/out_{date}.html")

        print("\n===== Done =====")
