# Downing JCR Comms Email Generator

> It is recommended to use the [single notebook version](https://gitlab.com/m-c-moore/downing-jcr-comms-email/-/tree/colab) of this program in [Google Colab](https://colab.research.google.com/).

This program can be used to generate a nice HTML email for the Downing JCR Comms newsletter. It was put together by [Matt Moore](https://gitlab.com/m-c-moore), and is based on [this project](https://github.com/dowjcr/comms-email) by [Cam O'Connor](https://github.com/cjoc). It can be installed and run through [main.py](/main.py), or cloned on [Azure](https://notebooks.azure.com/m-c-moore/projects/comms-email) and run through [main.ipynb](/main.ipynb). **The second method is recommended if you have no experience in coding.**

## Instructions

*The following can also be found in [main.ipynb](/main.ipynb), which includes additional examples*.

### Inputs

Please define the following variables:

- `intro = """..."""` - the introduction to the email. This can be formatted with HTML tags, and although the input can be written on multiple lines, **you must use `<br>` to specify line breaks**. Typically, this will look something like:

```html
    Hello <b>Downing!</b><br><br>
    Please see below for this week's opportunities.<br><br>
    Downing Love,<br><br>
    <b>Matt Moore</b><br>
```

- `meme_link = "..."` - the link to the meme. This must be a hyperlink to the public url of the meme image, and should end in something like `.png`,  `.jpeg` or `.gif`. Typically, this will look something like:

```html
    "https://www.jcr.dow.cam.ac.uk/storage/app/media/upload/comms/meme_m_7.png"
```

- `meme_caption = "..."` - the caption below the meme. This is optional, so if you would rather not have a caption, make sure the line reads `meme_caption = ""` (otherwise you will get an error).

In addition to these, you will need to **fill in `items.html` (in the root directory) with the newsletter entries**. Entries should be separated by `=====` (on its own line), and the first line of each item should be the title (other than that, line breaks are ignored). Like with the intro, this can be formatted with HTML tags, with `<br>` to specify line breaks.
 
### Running

Running the program will initialise the [Email Generator](/EmailGenerator.py) and write each section of the email to `out.html`. It will perform a few checks the hyperlinks of the bulletin items, and print any warnings.

### Output

Once the program has finished, the output in `out.html` should be copied into the email. **Previously generated emails can be found in the `archive` folder**.
